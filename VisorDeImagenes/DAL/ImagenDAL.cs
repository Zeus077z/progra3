﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisorDeImagenes.Entities;
using System.Drawing.Imaging;

namespace VisorDeImagenes.DAL
{
    class ImagenDAL
    {
        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            using (MemoryStream mStream = new MemoryStream(byteArrayIn))
            {
                return Image.FromStream(mStream);
            }
        }
        public void Insertar(EImagen reg)
        {

            string sql = " INSERT INTO public.visor_imagenes (imagen, tipo, titulo, descripcion) " +
                " VALUES (@png, @tip, @tit, @des) returning Id";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@png", ImageToByte(reg.Imagen));
                cmd.Parameters.AddWithValue("@tip", reg.Tipo);
                cmd.Parameters.AddWithValue("@tit", reg.Titulo);
                cmd.Parameters.AddWithValue("@des", reg.Descripcion);
                reg.Id = (int)cmd.ExecuteScalar();
            }
        }
        public List<EImagen> Fotos()
        {
            List<EImagen> registros = new List<EImagen>();
            string sql = "SELECT id, imagen, tipo, titulo, descripcion FROM public.visor_imagenes";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    registros.Add(CargarRegistros(reader));
                }
            }
            return registros;
        }
        private EImagen CargarRegistros(NpgsqlDataReader reader)
        {
            EImagen temp = new EImagen
            {
                Id = reader.GetInt32(0),
                Imagen = byteArrayToImage((byte[])reader["imagen"]),
                Tipo = reader.GetString(2),
                Titulo = reader.GetString(3),
                Descripcion = reader.GetString(4)
            };
            return temp;
        }
        public void Modificar(EImagen edit)
        {
           
            try
            {
                string sql = "UPDATE public.visor_imagenes SET imagen = @png , tipo = @tip, "
                    + "titulo = @tit, descripcion = @des "
                    + " WHERE id = @id";
                

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {

                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    MemoryStream ms = new MemoryStream();                   
                    edit.Imagen.Save(ms, ImageFormat.Jpeg);
                    cmd.Parameters.AddWithValue("@id", edit.Id);
                    cmd.Parameters.AddWithValue("@png", ms.ToArray());
                    cmd.Parameters.AddWithValue("@tip", edit.Tipo);
                    cmd.Parameters.AddWithValue("@des", edit.Descripcion);
                    cmd.Parameters.AddWithValue("@tit", edit.Titulo);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en update: " + ex);
            }
        }
        public void Eliminar(int id)
        {
            try
            {
                string sql = "DELETE FROM public.visor_imagenes WHERE id = @id";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en delete: " + ex);
            }
        }
    }
}
