﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisorDeImagenes.DAL;
using VisorDeImagenes.Entities;

namespace VisorDeImagenes.BOL
{
    class ImagenBOL
    {
        public void Registrar(EImagen re)
        {
            Validar(re);
            new ImagenDAL().Insertar(re);
        }
        public void Editar(EImagen re)
        {
            Validar(re);          
            new ImagenDAL().Modificar(re);
        }
        public ImagenDAL Dal()
        {
            return new ImagenDAL();
        }
        private void Validar(EImagen img)
        {
            if (string.IsNullOrWhiteSpace(img.Descripcion))
            {
                throw new Exception("Descripción necesaria");
            }
            if (string.IsNullOrWhiteSpace(img.Titulo))
            {
                throw new Exception("Titulo necesario");
            }
            if (string.IsNullOrWhiteSpace(img.Tipo))
            {
                throw new Exception("Tipo es necesario");
            }
        }
        public List<EImagen> Cargar()
        {
            return Dal().Fotos();

        }
    }
}
