﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisorDeImagenes.BOL;
using VisorDeImagenes.DAL;
using VisorDeImagenes.Entities;

namespace VisorDeImagenes.GUI
{
    public partial class FrmAdministrar : Form
    {
        private ImagenBOL Foto;
        private int cont;
        private ImagenDAL eliminar;
        public FrmAdministrar()
        {
            InitializeComponent();
            Foto = new ImagenBOL();
            eliminar = new ImagenDAL();
            lblError.Text = "";
        }
        private void PictureBox3_Click(object sender, EventArgs e)
        {
            cont++;
            lblError.Text = "";
            List<EImagen> imagenes = new List<EImagen>();
            imagenes = Foto.Cargar();
            pbAdelante.Enabled = true;
            try
            {
                txtTitulo.Text = imagenes[cont].Titulo;
                txtTipo.Text = imagenes[cont].Tipo;
                txtDescripcion.Text = imagenes[cont].Descripcion;
                pbImagenes.Image = imagenes[cont].Imagen;
            }
            catch (Exception)
            {
                cont = imagenes.Count - 1;
                pbAdelante.Enabled = false;
                pbAtras.Enabled = true;
            }
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            cont--;
            lblError.Text = "";
            List<EImagen> imagenes = new List<EImagen>();
            imagenes = Foto.Cargar();
            pbAtras.Enabled = true;
            try
            {
                txtTitulo.Text = imagenes[cont].Titulo;
                txtTipo.Text = imagenes[cont].Tipo;
                txtDescripcion.Text = imagenes[cont].Descripcion;
                pbImagenes.Image = imagenes[cont].Imagen;
            }
            catch(Exception)
            {
                cont = 0;
                pbAtras.Enabled = false;
                pbAdelante.Enabled = true;
            }                  
        }

        private void PbImagenes_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            DialogResult rs = dlgFile.ShowDialog();
            if (rs == DialogResult.OK)
            {
                pbImagenes.Image = Image.FromFile(dlgFile.FileName);
            }
        }
        public void Habilitar()
        {
            lblError.Text = "";
            txtTipo.ReadOnly = false;
            txtDescripcion.ReadOnly = false;
            txtTitulo.ReadOnly = false;

            txtTipo.BackColor = Color.White;
            txtDescripcion.BackColor = Color.White;
            txtTitulo.BackColor = Color.White;

            pbImagenes.Enabled = true;

            txtTipo.BorderStyle = BorderStyle.FixedSingle;
            txtDescripcion.BorderStyle = BorderStyle.FixedSingle;
            txtTitulo.BorderStyle = BorderStyle.FixedSingle;

            pbAtras.Visible = false;
            pbAdelante.Visible = false;
        }
        public void Deshabilitar()
        {
            lblError.Text = "";
            txtTipo.ReadOnly = true;
            txtDescripcion.ReadOnly = true;
            txtTitulo.ReadOnly = true;

            txtTipo.BackColor = Color.LightGray;
            txtDescripcion.BackColor = Color.LightGray;
            txtTitulo.BackColor = Color.LightGray;

            pbImagenes.Enabled = false;

            txtTipo.BorderStyle = BorderStyle.None;
            txtDescripcion.BorderStyle = BorderStyle.None;
            txtTitulo.BorderStyle = BorderStyle.None;

            pbAtras.Visible = true;
            pbAdelante.Visible = true;
        }
        public void Limpiar()
        {
            txtTipo.Text = "";
            txtDescripcion.Text = "";
            txtTitulo.Text = "";
            pbImagenes.Image = null;
        }

        private void Insertar_Click(object sender, EventArgs e)
        {
            Habilitar();
            Limpiar();
            lblError.Text = "";
            btnCargar.Visible = false;
            btnInsertar.Visible = true;
        }

        private void BtnInsertar_Click(object sender, EventArgs e)
        {
            
            try
            {
                lblError.Text = "";             
                EImagen b = new EImagen
                {                    
                    Imagen = pbImagenes.Image,
                    Tipo = txtTipo.Text.Trim(),
                    Titulo = txtTitulo.Text.Trim(),
                    Descripcion = txtDescripcion.Text.Trim()
                };
                Foto.Registrar(b);
                lblError.Text = "Se ha guardado correctamente la imagen!";
                lblError.ForeColor = Color.Green;
            }
            catch (Exception ex)
            {
                lblError.ForeColor = Color.IndianRed;
                lblError.Text = ex.Message;
            }

        }

        private void Modificar_Click(object sender, EventArgs e)
        {
            Habilitar();
            btnCargar.Visible = false;
            btnEditar.Visible = true;           
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                List<EImagen> imagenes = new List<EImagen>();
                imagenes = Foto.Cargar();
                EImagen b = new EImagen
                {
                    Id = imagenes[cont].Id,
                    Imagen = pbImagenes.Image,
                    Tipo = txtTipo.Text.Trim(),
                    Titulo = txtTitulo.Text.Trim(),
                    Descripcion = txtDescripcion.Text.Trim()
                };
                Foto.Editar(b);
                lblError.Text = "Se ha modificado correctamente!";
                lblError.ForeColor = Color.Green;
            }
            catch (Exception ex)
            {
                lblError.ForeColor = Color.IndianRed;
                lblError.Text = ex.Message;
            }
        }

        private void VisualizadorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Deshabilitar();
            lblError.Text = "";
            btnCargar.Visible = true;
            cont = 0;
            List<EImagen> imagenes = new List<EImagen>();
            imagenes = Foto.Cargar();
            txtTitulo.Text = imagenes[cont].Titulo;
            txtTipo.Text = imagenes[cont].Tipo;
            txtDescripcion.Text = imagenes[cont].Descripcion;
            pbImagenes.Image = imagenes[cont].Imagen;
        }

        private void BtnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                cont = 0;
                List<EImagen> imagenes = new List<EImagen>();
                imagenes = Foto.Cargar();
                txtTitulo.Text = imagenes[cont].Titulo;
                txtTipo.Text = imagenes[cont].Tipo;
                txtDescripcion.Text = imagenes[cont].Descripcion;
                pbImagenes.Image = imagenes[cont].Imagen;
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
           
        }

        private void Eliminar_Click(object sender, EventArgs e)
        {
            btnCargar.Visible = false;
            btnCargar.Visible = false;
            btnEliminar.Visible = true;

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            List<EImagen> imagenes = new List<EImagen>();
            imagenes = Foto.Cargar();
            eliminar.Eliminar(imagenes[cont].Id);
            lblError.Text = "Se ha eliminado correctamente!";
        }
    }
}
